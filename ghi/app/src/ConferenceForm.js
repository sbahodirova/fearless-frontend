import React, { useEffect, useState } from 'react';


function ConferenceForm () {

    const [locations, setLocations] = useState([])

    const[name, setName] = useState('')
    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }

    const[description, setDescription] = useState('')
    const handleDescriptionChange = (event) => {
        const value = event.target.value
        setDescription(value)
    }

    const[starts, setStarts] = useState('')
    const handleStartsChange = (event) => {
        const value = event.target.value
        setStarts(value)
    }

    const[ends, setEnds] = useState('')
    const handleEndsChange = (event) => {
        const value = event.target.value
        setEnds(value)
    }

    const[maxPresentations, setMaxPresentations] = useState('')
    const handleMaxPresentationsChange = (event) => {
        const value = event.target.value
        setMaxPresentations(value)
    }

    const[maxAttendees, setMaxAttendees] = useState('')
    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value
        setMaxAttendees(value)
    }

    const[location, setLocation] = useState('')
    const handleLocationChange = (event) => {
        const value = event.target.value
        setLocation(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.name = name
        data.description = description
        data.starts = starts
        data.ends = ends
        data.max_presentations = maxPresentations
        data.max_attendees = maxAttendees
        data.location = location

        console.log(data)

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference)

            setName('')
            setDescription('')
            setStarts('')
            setEnds('')
            setMaxPresentations('')
            setMaxAttendees('')
            setLocation('')
        }
    }

    const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        setLocations(data.locations)

    }
  }

    useEffect(() => {
        fetchData();
    }, []);



    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" id="name" className="form-control" name="name" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description">Description</label>
                <textarea onChange={handleDescriptionChange} value={description} required type="text" id="description" className="form-control" name="description"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartsChange} value={starts} placeholder="Starts" required type="date" id="starts" className="form-control" name="starts" />
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndsChange} value={ends} placeholder="Ends" required type="date" id="ends" className="form-control" name="ends" />
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxPresentationsChange} value={maxPresentations} placeholder="Max Presentations" required type="number" id="max_presentations" className="form-control" name="max_presentations" />
                <label htmlFor="max_presentations">Max Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxAttendeesChange} value={maxAttendees} placeholder="Max Attendees" required type="number" id="max_attendees" className="form-control" name="max_attendees" />
                <label htmlFor="max_attendees">Max Attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} value={location} required id="location" className="form-select" name="location">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                    <option key={location.id} value={location.id}>
                        {location.name}
                    </option>
                    );
                })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm;
