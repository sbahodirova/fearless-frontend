window.addEventListener('DOMContentLoaded', async () => {

    const payloadCookie = cookieStore.get("jwt_access_payload");
    if (payloadCookie) {
    const encodedPayload = JSON.parse(payloadCookie.value);
    const decodedPayload = atob(encodedPayload);
    const payload = JSON.parse(decodedPayload);
    console.log(payload);
    if (payload.permissions.includes("events.add_conference")) {
        const link = document.querySelector("#new-conference-link");
        link.classList.remove("d-none");
    }
    if (payload.permissions.includes("events.add_location")) {
        const link = document.querySelector("#new-location-link");
        link.classList.remove("d-none");
    }
    if (payload.permissions.includes("events.view_conference")) {
        const link = document.querySelector("#new-location-link");
        link.classList.remove("d-none");
    }
    }
})


