function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
      <div class="card mb-3 shadow" aria-hidden="true">
        <img src="${pictureUrl}" class="card-img-top">
          <div class="card-body">
            <h5 class="card-title placeholder-glow">${name}
              <span class="placeholder col"></span>
            </h5>
            <h6 class="card-subtitle text-muted placeholder-glow">${location}
              <span class="placeholder col"></span>
            </h6>
            <p class="card-text placeholder-glow">${description}
              <span class="placeholder col"></span>
            </p>
          </div>
        <div class="card-footer placeholder-glow">
          ${new Date(starts).toLocaleDateString()} -
          ${new Date(ends).toLocaleDateString()}
        </div>
      </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);

        if (!response.ok) {
          var alertList = document.querySelectorAll('.alert')
          var alerts =  [].slice.call(alertList).map(function (element) {
            return new bootstrap.Alert(element)
          })
        } else {
            const data = await response.json();
          let index = 0;
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name
                    const description = details.conference.description
                    const starts = details.conference.starts
                    const ends = details.conference.ends
                    const location = details.conference.location.name
                    const pictureUrl = details.conference.location.picture_url
                    const html = createCard(name, description, pictureUrl, starts, ends, location)
                    const column = document.querySelector(`#col-${index % 3}`);
                    column.innerHTML += html;
                    index += 1;
                }
            }
        }
    } catch (e) {
      console.error(e)
        console.error('error', error);
    }

});
